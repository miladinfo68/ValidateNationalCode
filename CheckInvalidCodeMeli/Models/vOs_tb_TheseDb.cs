//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace CheckInvalidCodeMeli.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class vOs_tb_TheseDb
    {
        public decimal id_os { get; set; }
        public string name_os { get; set; }
        public string famili_os { get; set; }
        public string resh { get; set; }
        public Nullable<decimal> magta { get; set; }
        public Nullable<decimal> rotbe { get; set; }
        public Nullable<decimal> groh { get; set; }
        public Nullable<int> zarfyat_mosh { get; set; }
        public string pass { get; set; }
        public Nullable<int> tayed { get; set; }
        public string code_meli { get; set; }
        public string savabegh { get; set; }
        public Nullable<int> semat { get; set; }
        public Nullable<int> zarfyat_rah { get; set; }
        public string addres_os { get; set; }
        public string mob_os { get; set; }
        public Nullable<int> fall { get; set; }
        public string mail { get; set; }
        public string date_sabt { get; set; }
        public string time_sabt { get; set; }
        public Nullable<int> payeh { get; set; }
        public Nullable<int> type_ham { get; set; }
        public string code_sazmani { get; set; }
        public Nullable<int> groh_karbari { get; set; }
        public Nullable<int> ozv_shora { get; set; }
        public string file_madarek { get; set; }
        public Nullable<int> jens { get; set; }
        public string pic_os { get; set; }
        public Nullable<int> tayed_os { get; set; }
        public string num_hesab { get; set; }
        public Nullable<int> bank_id { get; set; }
        public string mahal_t { get; set; }
        public string tarikh_t { get; set; }
        public string mahal_sodor { get; set; }
        public Nullable<int> mahal_sodor_ost { get; set; }
        public Nullable<int> lock_madr { get; set; }
        public string sh_shn { get; set; }
        public string bimeh { get; set; }
        public string mahal_estekhdam { get; set; }
        public string FaName { get; set; }
        public int ZarfyatDav { get; set; }
    }
}
